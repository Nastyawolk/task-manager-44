package ru.t1.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull EntityManager getEntityManager();

    @NotNull
    EntityManagerFactory factory();

}
