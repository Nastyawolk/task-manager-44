package ru.t1.volkova.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.IDatabaseProperty;
import ru.t1.volkova.tm.dto.model.ProjectDTO;
import ru.t1.volkova.tm.dto.model.SessionDTO;
import ru.t1.volkova.tm.dto.model.TaskDTO;
import ru.t1.volkova.tm.dto.model.UserDTO;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.model.Session;
import ru.t1.volkova.tm.model.Task;
import ru.t1.volkova.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IDatabaseProperty databaseProperties;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(
            @NotNull final IDatabaseProperty databaseProperties
    ) {
        this.databaseProperties = databaseProperties;
        this.entityManagerFactory = factory();
    }

    @Override
    @NotNull
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @Override
    @NotNull
    public EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, databaseProperties.getDatabaseDriver());
        settings.put(org.hibernate.cfg.Environment.URL, databaseProperties.getDatabaseUrl());
        settings.put(org.hibernate.cfg.Environment.USER, databaseProperties.getDatabaseUsername());
        settings.put(org.hibernate.cfg.Environment.PASS, databaseProperties.getDatabasePassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, databaseProperties.getDatabaseDialect());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, databaseProperties.getDatabaseHbm2ddlAuto());
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, databaseProperties.getDatabaseShowSql());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(Session.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(User.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
