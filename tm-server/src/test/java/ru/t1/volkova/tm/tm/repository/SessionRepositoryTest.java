package ru.t1.volkova.tm.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.dto.model.SessionDTO;
import ru.t1.volkova.tm.repository.dto.SessionDTORepository;
import ru.t1.volkova.tm.service.ConnectionService;
import ru.t1.volkova.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 3;

    @NotNull
    private List<SessionDTO> sessionList;

    @NotNull
    private List<String> userIdList;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    final EntityManager entityManager = connectionService.getEntityManager();

    @NotNull
    final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);

    @Before
    public void initRepository() {
        sessionList = new ArrayList<>();
        userIdList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull SessionDTO session = new SessionDTO();
            session.setRole(Role.USUAL);
            @NotNull String userId = UUID.randomUUID().toString();
            userIdList.add(userId);
            session.setUserId(userId);
            sessionRepository.add(session);
            sessionList.add(session);
        }
    }

    @Test
    public void testAddSession() {
        @NotNull final Random random = new Random();
        @NotNull final String userId = userIdList.get(random.nextInt(userIdList.size()));
        int expectedNumberOfEntries = sessionRepository.getSize(userId) + 1;
        @NotNull final SessionDTO newSession = new SessionDTO();
        newSession.setUserId(userId);
        sessionRepository.add(newSession);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(userId));
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final String userId1 = userIdList.get(0);
        @NotNull final String userId2 = userIdList.get(2);
        @Nullable final List<SessionDTO> sessionList = sessionRepository.findAll(userId1);
        @Nullable final List<SessionDTO> sessionList2 = sessionRepository.findAll(userId2);
        if (sessionList != null && sessionList2 != null) {
            Assert.assertEquals(sessionRepository.getSize(userId1), sessionList.size());
            Assert.assertEquals(sessionRepository.getSize(userId2), sessionList2.size());
        }
    }

    @Test
    public void testFindAllForUserNegative() {
        @Nullable final List<SessionDTO> sessionList = sessionRepository.findAll((String) null);
        Assert.assertEquals(0, sessionList.size());
        @Nullable final List<SessionDTO> sessionList2 = sessionRepository.findAll("non-existent-id");
        if (sessionList2 == null) return;
        Assert.assertEquals(0, sessionList2.size());
    }

    @Test
    public void testFindOneByIdForUser() {
        for (@NotNull final SessionDTO session : sessionList) {
            assertEquals(session, sessionRepository.findOneById(session.getUserId(), session.getId()));
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        @NotNull final String userId1 = userIdList.get(0);
        @NotNull final String userId2 = userIdList.get(2);
        Assert.assertNull(sessionRepository.findOneById(null, sessionList.get(1).getId()));
        Assert.assertNull(sessionRepository.findOneById(userId1, "NotExcitingId"));
        Assert.assertNull(sessionRepository.findOneById(userId2, "NotExcitingId"));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final Random random = new Random();
        @NotNull final String userId = userIdList.get(random.nextInt(userIdList.size()));
        @Nullable final SessionDTO session = sessionRepository.findOneByIndex(userId, 0);
        Assert.assertNotNull(session);
    }

    @Test
    public void testFindOneByIndexForUserNegative() {
        @NotNull final Random random = new Random();
        @NotNull final String userId = userIdList.get(random.nextInt(userIdList.size()));
        Assert.assertNull(sessionRepository.findOneByIndex(userId, NUMBER_OF_ENTRIES + 10));
    }

    @Test
    public void testRemoveOneForUser() {
        @NotNull final String userId1 = userIdList.get(0);
        @Nullable final SessionDTO session1 = sessionRepository.findOneByIndex(userId1, 0);
        sessionRepository.removeOneById(userId1, session1.getId());
        Assert.assertEquals(0, sessionRepository.getSize(userId1));
    }

    @Test
    public void testRemoveOneByIdForUser() {
        @NotNull final String userId1 = userIdList.get(0);
        @NotNull final String id = sessionList.get(0).getId();
        sessionRepository.removeOneById(userId1, id);
        Assert.assertEquals(0, sessionRepository.getSize(userId1));
    }

    @Test
    public void testRemoveAllForUser() {
        @NotNull final String userId1 = userIdList.get(0);
        @NotNull final String userId2 = userIdList.get(1);
        sessionRepository.clear(userId1);
        sessionRepository.clear(userId2);
        Assert.assertEquals(0, sessionRepository.getSize(userId1));
        Assert.assertEquals(0, sessionRepository.getSize(userId2));
    }

    @Test
    public void testRemoveAllForUserNegative() {
        @NotNull final String userId1 = userIdList.get(2);
        @NotNull final String userId2 = userIdList.get(1);
        sessionRepository.clear("NotExcitingId");
        sessionRepository.clear((String) null);
        Assert.assertNotEquals(0, sessionRepository.getSize(userId1));
        Assert.assertNotEquals(0, sessionRepository.getSize(userId2));
    }

    @Test
    public void testRemoveOneByIndexForUser() {
        @NotNull final Random random = new Random();
        @NotNull final String userId = userIdList.get(random.nextInt(userIdList.size()));
        System.out.println(sessionRepository.getSize(userId));
        sessionRepository.removeOneByIndex(userId, 0);
        Assert.assertEquals(0, sessionRepository.getSize(userId));
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final String userId1 = userIdList.get(2);
        @NotNull final String userId2 = userIdList.get(1);
        Assert.assertNotEquals(0, sessionRepository.getSize(userId1));
        Assert.assertNotEquals(0, sessionRepository.getSize(userId2));
    }

}
