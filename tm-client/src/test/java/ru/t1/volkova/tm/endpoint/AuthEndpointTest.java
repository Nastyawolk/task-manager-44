package ru.t1.volkova.tm.endpoint;

import javax.xml.ws.soap.SOAPFaultException;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.volkova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.volkova.tm.dto.request.user.UserLoginRequest;
import ru.t1.volkova.tm.dto.request.user.UserLogoutRequest;
import ru.t1.volkova.tm.dto.response.user.UserLoginResponse;
import ru.t1.volkova.tm.dto.response.user.UserLogoutResponse;
import ru.t1.volkova.tm.marker.SoapCategory;

import java.sql.SQLException;

public class AuthEndpointTest {

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstanse();

    @Nullable
    private String token;

    @Before
    public void initTest() throws SQLException {
        @NotNull final String login = "admin";
        @NotNull final String password = "admin";
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        token = authEndpoint.login(request).getToken();
    }

    @Test
    @Category(SoapCategory.class)
    public void testLogin() throws SQLException {
        @NotNull final String login = "admin";
        @NotNull final String password = "admin";
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(request);
        Assert.assertTrue(userLoginResponse.getSuccess());
    }

    @Test(expected = SOAPFaultException.class)
    @Category(SoapCategory.class)
    public void testLoginNegative() throws SQLException {
        @NotNull final String login = "non-exist";
        @NotNull final String password = "admin";
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        authEndpoint.login(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testLogout() throws Exception {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(token);
        @NotNull final UserLogoutResponse userLogoutResponse = authEndpoint.logout(request);
        Assert.assertTrue(userLogoutResponse.getSuccess());
    }

    @Test(expected = SOAPFaultException.class)
    @Category(SoapCategory.class)
    public void testLogoutNegative() throws Exception {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest("non-exist");
        authEndpoint.logout(request);
    }

}
